<?php 
// memanggil file siswa.php
require_once 'penduduk.php';

if (isset($_POST['submit'])) {
    // membuat objek siswa
    $penduduk = new penduduk();
    
    // ambil data hasil submit dari form
    $nik           = $_POST['nik'];
    $nama          = $_POST['nama'];
    $jenis_kelamin = $_POST['jenis_kelamin'];
    $status_kawin  = $_POST['status_kawin'];
    $pekerjaan     = $_POST['pekerjaan'];
    $kewarganegaraan= $_POST['kewarganegaraan'];
    $tempat_lahir  = $_POST['tempat_lahir'];
    $tanggal       = $_POST['tanggal_lahir'];
    $gol_darah     = $_POST['gol_darah'];


    // insert data penduduk
    $penduduk->insert($nik,$nama,$jenis_kelamin,$status_kawin, $pekerjaan, $kewarganegaraan, $tempat_lahir,$tanggal, $gol_darah);
}			


?>