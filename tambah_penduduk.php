<?php

$page = "home";
require 'layout/header.php';


?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Halaman Penduduk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Input Data Penduduk</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="proses_tambah.php" method="POST">
                  <div class="card-body">
                  <div class="form-group">
                      <label for="nik">NIK</label>
                      <input type="number" class="form-control" name="nik" id="nik" placeholder="Masukan NIK Anda" required />
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama Anda" required>
                    </div>
                    <div class="form-group">
                      <label for="jenis-kelamin">Jenis Kelamin</label>
                      <select id="jenis_kelamin" name="jenis_kelamin" class="form-control">
                        <option value="" disabled hidden selected>Pilih jenis-kelamin</option>
                        <option value="Laki-laki">Laki-laki</option>
                        <option value="Perempuan">Perempuan</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="status_kawin">Status Perkawinan</label>
                      <select id="status_kawin" name="status_kawin" class="form-control">
                        <option value="" disabled hidden selected>Pilih status perkawinan</option>
                        <option value="kawin">Kawin</option>
                        <option value="belum_kawin">Belum Kawin</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="pekerjaan">pekerjaan</label>
                      <input type="text" class="form-control" name ="pekerjaan" id="pekerjaan" placeholder="Masukkan pekerjaan Anda!" required>
                    </div>
                    <div class="form-group">
                      <label for="kewarganegaraan">Status Kewarganegaraan</label>
                      <select id="kewarganegaraan" name="kewarganegaraan" class="form-control">
                        <option value="" disabled hidden selected>Pilih status Kewarganegaraan</option>
                        <option value="WNI">WNI</option>
                        <option value="WNA">WNA</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="tempat-lahir">tempat Lahir</label>
                      <input type="text" class="form-control" name="tempat_lahir" id="tempat-lahir" placeholder="Masukan tempat Lahir Anda!" required>
                    </div>
                    <div class="form-group">
                      <label for="tanggal-lahir">Tanggal Lahir</label>
                      <input type="date" class="form-control date-picker" name="tanggal_lahir" id="tanggal-lahir" placeholder="Masukan Tanggal Lahir Anda!" required >
                    </div>
                    <div class="form-group">
                      <label for="gol_darah">Status golongan darah</label>
                      <select id="gol_darah" name="gol_darah" class="form-control">
                        <option value="" disabled hidden selected>Pilih gol-darah</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="AB">AB</option>
                        <option value="O">O</option>
                      </select>
                    </div>
                  </div>
                  <!-- /.card-body -->
  
                  <div class="card-footer">
                    <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                    <a href="index.php" class="btn btn-default btn-reset">Batal</a>
                  </div>
                </form>
              </div>
              <!-- /.card -->
             
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php 
  require 'layout/footer.php';

?>