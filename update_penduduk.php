<?php

$page = "home";
require 'layout/header.php';

$nik = $_GET['id'];

if (isset($nik)) {
    // memanggil file penduduk.php
    require_once 'penduduk.php';

    // membuat objek penduduk
    $penduduk = new penduduk();

    // mengambil data penduduk
    $data = $penduduk->get_penduduk($nik);
    //var_dump($data);
    
    //ambil data hasil submit dari form
    $nik           = $data['nik'];
    $nama          = $data['nama'];
    $jenis_kelamin = $data['jenis_kelamin'];
    $status_kawin  = $data['status_perkawinan'];
    $pekerjaan     = $data['pekerjaan'];
    $kewarganegaraan= $data['kewarganegaraan'];
    $tempat_lahir  = $data['tempat_lahir'];
    $tanggal       = $data['tanggal_lahir'];
    $gol_darah     = $data['gol_darah'];

}			
?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Halaman Penduduk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Input Data Penduduk</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="proses_ubah.php" method="POST">
                  <div class="card-body">
                  <div class="form-group">
                      <label for="nik">NIK</label>
                      <input type="number" class="form-control" name="nik" id="nik" value="<?= $data['nik']; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" name="nama" autocomplete="off" id="nama" value="<?= $nama; ?> "> 
                    </div>
                    <div class="form-group">
                      <label for="jenis-kelamin">Jenis Kelamin</label>
                      <select id="jenis_kelamin" name="jenis_kelamin" class="form-control">
                       <option autocomplete="off"value="<?= $jenis_kelamin; ?>"><?= $jenis_kelamin; ?></option>
                        <option value="Laki-laki">Laki-laki</option>
                        <option value="Perempuan">Perempuan</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="status_kawin">Status Perkawinan</label>
                      <select id="status_kawin" name="status_kawin" class="form-control">
                       <option value="<?= $status_kawin; ?>"><?= $status_kawin; ?></option>
                        <option value="kawin">Kawin</option>
                        <option value="belum_kawin">Belum Kawin</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="pekerjaan">pekerjaan</label>
                      <input type="text" class="form-control" name ="pekerjaan" id="pekerjaan" value="<?= $pekerjaan; ?>">
                    </div>
                    <div class="form-group">
                      <label for="kewarganegaraan">Status Kewarganegaraan</label>
                      <select id="kewarganegaraan" name="kewarganegaraan" class="form-control">
                        <option value="<?= $kewarganegaraan; ?>"><?= $kewarganegaraan; ?></option>
                        <option value="WNI">WNI</option>
                        <option value="WNA">WNA</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="tempat-lahir">tempat Lahir</label>
                      <input type="text" class="form-control" name="tempat_lahir" id="tempat-lahir" value="<?= $tempat_lahir; ?>">
                    </div>
                    <div class="form-group">
                      <label for="tanggal-lahir">Tanggal Lahir</label>
                      <input type="date" class="form-control" name="tanggal_lahir" id="tanggal-lahir" value="<?= $tanggal; ?>">
                    </div>
                    <div class="form-group">
                      <label for="gol_darah">Status golongan darah</label>
                      <select id="gol_darah" name="gol_darah" class="form-control">
                        <option value="<?= $gol_darah; ?>"><?= $gol_darah ?></option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="AB">AB</option>
                        <option value="O">O</option>
                      </select>
                    </div>
                  </div>
                  <!-- /.card-body -->
  
                  <div class="card-footer">
                    <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                    <a href="index.php" class="btn btn-default btn-reset">Batal</a>
                  </div>
                </form>
              </div>
              <!-- /.card -->
             
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php 
  require 'layout/footer.php';

?>