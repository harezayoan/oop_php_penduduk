<?php
// memanggil file penduduk.php
require_once 'penduduk.php';
    
if (isset($_POST['submit'])) {
	if (isset($_POST['nik'])) {

		// membuat objek penduduk
    	$penduduk = new penduduk();

    	// ambil data hasil submit dari form
		$nik           = $_POST['nik'];
        $nama          = $_POST['nama'];
        $jenis_kelamin = $_POST['jenis_kelamin'];
        $status_kawin  = $_POST['status_kawin'];
        $pekerjaan     = $_POST['pekerjaan'];
        $kewarganegaraan= $_POST['kewarganegaraan'];
        $tempat_lahir  = $_POST['tempat_lahir'];
        $tanggal       = $_POST['tanggal_lahir'];
        $gol_darah     = $_POST['gol_darah'];

		// update data penduduk
    	$penduduk->update($nik, $nama, $jenis_kelamin, $status_kawin, $pekerjaan, $kewarganegaraan, $tempat_lahir,$tanggal, $gol_darah);		
	}
}					
?>