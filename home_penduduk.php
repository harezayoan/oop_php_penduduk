<?php 
  $page = "home";
  require 'layout/header.php';
 
?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Halaman Penduduk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Data Penduduk</h3>
                </div>
                <?php  
                  if (empty($_GET['alert'])) {
                    echo "";
                  } elseif ($_GET['alert'] == 1) {
                    echo "<div class='alert alert-danger alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                              <span aria-hidden='true'>&times;</span>
                            </button>
                            <strong><i class='glyphicon glyphicon-alert'></i> Gagal!</strong> Terjadi kesalahan.
                          </div>";
                  } elseif ($_GET['alert'] == 2) {
                    echo "<div class='alert alert-success alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                              <span aria-hidden='true'>&times;</span>
                            </button>
                            <strong><i class='glyphicon glyphicon-ok-circle'></i> Sukses!</strong> Data penduduk berhasil disimpan.
                          </div>";
                  } elseif ($_GET['alert'] == 3) {
                    echo "<div class='alert alert-success alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                              <span aria-hidden='true'>&times;</span>
                            </button>
                            <strong><i class='glyphicon glyphicon-ok-circle'></i> Sukses!</strong> Data penduduk berhasil diubah.
                          </div>";
                  } elseif ($_GET['alert'] == 4) {
                    echo "<div class='alert alert-success alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                              <span aria-hidden='true'>&times;</span>
                            </button>
                            <strong><i class='glyphicon glyphicon-ok-circle'></i> Sukses!</strong> Data penduduk berhasil dihapus.
                          </div>";
                  }
                  ?>
                    <div class="card-body">
                        <table class="table table-bordered">
                        <thead>
                            <tr>
                                <!-- <th>No</th> -->
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Status</th>
                                <th>Pekerjaan</th>
                                <th>Kewarga - negaraan</th>
                                <th>Tempat lahir</th>
                                <th>Tanggal lahir</th>
                                <th>Gol. darah</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        // memanggil file siswa.php
                        require_once 'penduduk.php';
                        
                        // membuat objek siswa
                        $penduduk = new penduduk();
                        
                        // mengambil seluruh data siswa
                        $result = $penduduk->view();
                        $no = 1;          
                            foreach($result as $row) {
                            ?>
                            <tr>
                                <!-- <td><?= $no++; ?></td> -->
                                <td><?= $row['nik']; ?></td>
                                <td><?= $row['nama']; ?></td>
                                <td><?= $row['jenis_kelamin']; ?></td>
                                <td><?= $row['status_perkawinan']; ?></td>
                                <td><?= $row['pekerjaan']; ?></td>
                                <td><?= $row['kewarganegaraan']; ?></td>
                                <td><?= $row['tempat_lahir']; ?></td>
                                <td><?= $row['tanggal_lahir']; ?></td>
                                <td><?= $row['gol_darah']; ?></td>
                                <td>
                                  <a href="update_penduduk.php?id=<?= $row['nik']; ?>" class="btn btn-success btn-sm"><i class="bi bi-pencil-square"></i></a>
                                  <a class="btn btn-danger btn-sm" href="proses_hapus.php?id=<?= $row['nik'];?>" onclick="return confirm('Anda yakin ingin menghapus data <?= $row['nama']; ?>?');">
                                  <i class="bi bi-trash-fill"></i>
                                  </a>
                                </td>
                            </tr>
                            <?php  } ?>
                        </tbody>
                        </table>
                    </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
              <a href="tambah_penduduk.php" class="btn btn-primary"><i class="bi bi-person-plus-fill"></i> Tambah</a>
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
            <!-- /.card -->
              </div>
              <!-- /.card -->
             
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php 
  require 'layout/footer.php';
?>
