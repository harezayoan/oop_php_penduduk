<?php 
class penduduk{
    function view(){
        // memanggil file database.php
        require_once "config/database.php";

        // membuat objek db dengan nama $db
        $db = new database();

        // membuka koneksi ke database
        $mysqli = $db->connect();

        // sql statement untuk mengambil semua data penduduk
        $sql = "SELECT * FROM penduduk ORDER BY id_penduduk";

        $result = $mysqli->query($sql);

        while ($data = $result->fetch_assoc()) {
            $hasil[] = $data;
        }

        // menutup koneksi database
        $mysqli->close();

        // nilai kembalian dalam bentuk array
        return $hasil;

    }

     /* Method untuk menyimpan data ke tabel penduduk */
     function insert($nik,$nama,$jenis_kelamin,$status_kawin, $pekerjaan, $kewarganegaraan, $tempat_lahir,$tanggal_lahir, $gol_darah) {
        // memanggil file database.php
        require_once "config/database.php";

        // membuat objek db dengan nama $db
        $db = new database();

        // membuka koneksi ke database
        $mysqli = $db->connect();

        // sql statement untuk insert data penduduk
        $sql = "INSERT INTO penduduk (nik,nama,jenis_kelamin,status_perkawinan, pekerjaan, kewarganegaraan, tempat_lahir,tanggal_lahir, gol_darah) 
                VALUES ('$nik','$nama','$jenis_kelamin','$status_kawin', '$pekerjaan', '$kewarganegaraan', '$tempat_lahir','$tanggal_lahir', '$gol_darah')";

        $result = $mysqli->query($sql);

        // cek hasil query
        if($result){
            /* jika data berhasil disimpan alihkan ke halaman penduduk dan tampilkan pesan = 2 */
            header("Location: home_penduduk.php?alert=2");
        }
        else{
            /* jika data gagal disimpan alihkan ke halaman penduduk dan tampilkan pesan = 1 */
            header("Location: home_penduduk.php?alert=1");
        }

        // menutup koneksi database
        $mysqli->close();
    }

    /* Method untuk menampilkan data penduduk berdasarkan penduduk */
    function get_penduduk($nik) {
        // memanggil file database.php
        require_once "config/database.php";

        // membuat objek db dengan nama $db
        $db = new database();

        // membuka koneksi ke database
        $mysqli = $db->connect();

        // sql statement untuk mengambil data penduduk berdasarkan penduduk
        $sql = "SELECT * FROM penduduk WHERE nik='$nik'";

        $result = $mysqli->query($sql);
        $data   = $result->fetch_assoc();

        // menutup koneksi database
        $mysqli->close();
        
        // nilai kembalian dalam bentuk array
        return $data;
    }

    /* Method untuk menyimpan data ke tabel penduduk */
    function update($nik, $nama, $jenis_kelamin, $status_kawin, $pekerjaan, $kewarganegaraan, $tempat_lahir,$tanggal_lahir, $gol_darah) {
        // memanggil file database.php
        require_once "config/database.php";

        // membuat objek db dengan nama $db
        $db = new database();

        // membuka koneksi ke database
        $mysqli = $db->connect();

        // sql statement untuk update data penduduk
        $sql = "UPDATE penduduk SET nama='$nama',jenis_kelamin='$jenis_kelamin',status_perkawinan='$status_kawin', pekerjaan='$pekerjaan', kewarganegaraan='$kewarganegaraan', tempat_lahir='$tempat_lahir',tanggal_lahir='$tanggal_lahir', gol_darah='$gol_darah' WHERE nik = '$nik'";

        $result = $mysqli->query($sql);

        // cek hasil query
        if($result){
            /* jika data berhasil disimpan alihkan ke halaman penduduk dan tampilkan pesan = 2 */
            header("Location: home_penduduk.php?alert=2");
        }
        else{
            /* jika data gagal disimpan alihkan ke halaman penduduk dan tampilkan pesan = 1 */
            header("Location: home_penduduk.php?alert=1");
        }

        // menutup koneksi database
        $mysqli->close();
    }

    function delete($nik) {
        // memanggil file database.php
        require_once "config/database.php";

        // membuat objek db dengan nama $db
        $db = new database();

        // membuka koneksi ke database
        $mysqli = $db->connect();

        // sql statement untuk delete data penduduk
        $sql = "DELETE FROM penduduk WHERE nik = '$nik'";

        $result = $mysqli->query($sql);

        // cek hasil query
        if($result){
            /* jika data berhasil disimpan alihkan ke halaman penduduk dan tampilkan pesan = 4 */
            header("Location: home_penduduk.php?alert=4");
        }
        else{
            /* jika data gagal disimpan alihkan ke halaman penduduk dan tampilkan pesan = 1 */
            header("Location: home_penduduk.php?alert=1");
        }

        // menutup koneksi database
        $mysqli->close();
    }
}




?>